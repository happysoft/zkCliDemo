# zkCliDemo

#### 项目介绍
zookeeper客户端演示程序

#### 软件架构
1. 基于原生zookeeper的API客户端
2. 基于开源Curator客户端


#### 安装教程

1. 工程导入到eclipse

#### 使用说明

1. AclCreateWatcher权限控制演示
2. ZkClientAsyncWatcher异步创建节点模式
3. ZkClientSyncWatcher同步创建节点模式
4. ZkCuratorClientTest/ZkCuratorWacther--Curator客户端调用演示

